package com.gitee.easyopen.server.api.param;

import com.gitee.easyopen.doc.annotation.ApiDocField;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class GoodsParam4 {

    @ApiDocField(description = "id", required = true, example = "11")
    @NotNull(message = "id不能为空")
    private Integer id;

    @ApiDocField(description = "商品数组", required = true, elementClass = GoodsParam2.class)
    @NotEmpty(message = "数组不能为空")
    private List<GoodsParam2> list;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<GoodsParam2> getList() {
        return list;
    }

    public void setList(List<GoodsParam2> list) {
        this.list = list;
    }
}
