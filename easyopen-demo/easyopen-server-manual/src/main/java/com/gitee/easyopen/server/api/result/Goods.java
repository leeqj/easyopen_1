package com.gitee.easyopen.server.api.result;

import java.math.BigDecimal;

import com.gitee.easyopen.doc.DataType;
import com.gitee.easyopen.doc.annotation.ApiDocField;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("goods")
public class Goods {

    @ApiDocField(description = "id", example = "1")
    private Long id;
    @ApiDocField(description = "商品名称", maxLength = "64", example = "apple")
    private String goods_name;
    @ApiDocField(description = "价格", dataType = DataType.FLOAT, example = "7999")
    private BigDecimal price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGoods_name() {
        return goods_name;
    }

    public void setGoods_name(String goods_name) {
        this.goods_name = goods_name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Goods [id=" + id + ", goods_name=" + goods_name + ", price=" + price + "]";
    }

}
